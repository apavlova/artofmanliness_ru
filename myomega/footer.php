<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the class=site-inner div and all content after
 *
 * @package Omega
 */
?>
		<?php do_action( 'omega_after_main' ); ?>
	</div><!-- .site-inner -->
	<?php 
	do_action( 'omega_before_footer' ); 
	do_action( 'omega_footer' ); 
	do_action( 'omega_after_footer' ); 
	?>
</div><!-- .site-container -->
<?php do_action( 'omega_after' ); ?>
<?php wp_footer(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script type='text/javascript'>
	  var f=0;
		$(document).ready(function(){

		$("#menu-icon").on("click", function() {
		if(f==0)   {
		     $("#menu-mainmenu").css({display : "block"});
		     f=1;
		  } else  {
		     $("#menu-mainmenu").css({display : "none"});
		     f=0;
		  }
		});
	   });
</script>
</body>
</html>