<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Omega
 */

get_header(); ?>
<div class="new-sidebar">
	<a href="index.php?random=1" class="new-sidebar-btn"><h4>Рандом</h4></a>
	<?php 
	$infocat = get_the_category();
	$cat_id = $infocat[0]->cat_ID; 
	$catlink = get_category_link( $cat_id );
	echo('<a href="'.$catlink.'" class="new-sidebar-btn"><h4>Новое</h4></a>');?>
	<a href="../../../../index.php?popular=<?php echo $infocat[0]->slug; ?>" class="new-sidebar-btn"><h4>Популярное</h4></a>
</div>
<main class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
	<?php
	do_action( 'omega_before_content' );
	do_action( 'omega_content' );
	do_action( 'omega_after_content' );
	?>
</main><!-- .content -->
<?php get_footer(); ?>