<?php
echo('<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">');
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Omega
 */

get_header(); 

if(get_query_var('popular')=="" || get_query_var('popular')=="1")
{?>
<!--Область навигации для media-->	
<div class="new-sidebar">
	<a href="index.php?random=2" class="new-sidebar-btn"><h4>Рандом</h4></a>
	<a href="/wordpress" class="new-sidebar-btn"><h4>Новое</h4></a>
	<a href="index.php?popular=1" class="new-sidebar-btn"><h4>Популярное</h4></a>
</div>
<?php
}
else{?>
<div class="new-sidebar">
	<a href="index.php?random=<?php echo get_query_var('popular');?>" class="new-sidebar-btn"><h4>Рандом</h4></a>
	<a href="category/<?php echo get_query_var('popular');?>" class="new-sidebar-btn"><h4>Новое</h4></a>
	<a href="index.php?popular=<?php echo get_query_var('popular');?>" class="new-sidebar-btn"><h4>Популярное</h4></a>
</div>
<?php }?> 
<main class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" 
	<?php omega_attr( 'content' ); ?>>
	<?php 
	do_action( 'omega_before_content' );
	//do_action( 'omega_content' ); 
	/* !!!Стандартный вывод популярного */
		if (get_query_var('popular')){
				echo("<h3>Популярное</h3>");
		$mycat = get_category_by_slug(get_query_var('popular'));
		if($mycat->cat_ID != 0)
		{
			echo("<h4>" . $mycat->name ."</h4>");
			$shortcode_ops = array(
				'cat' => $mycat->cat_ID,
				'range' => 'all',
            	'order_by' => 'views');
		}
		else
			$shortcode_ops = array(
				'range' => 'all',
            	'order_by' => 'views');			
		$popular = new WPP_Query($shortcode_ops);
		$popular_posts = $popular->get_posts();
		$cnt = sizeof($popular_posts);
		//var_dump($popular_posts);
		echo '<div class="postBox-main">';
		$col = 'one';
		for($i=0; $i<$cnt; $i++){
			echo ('<article class="postBox ' .$col. '">');
			if ($col=='one')$col='two';
			else $col='one';
			
			$post_id = $popular_posts[$i]->id;
			//echo get_the_title( $post_id );
			?>
			<div class="postThumb"><a href="<?php the_permalink($post_id)?>"><?php MultiPostThumbnails::the_post_thumbnail(get_post_type($post_id),'secondary-image', $post_id, 'post-list-img')?></a></div>
			<h2><a href="<?php the_permalink($post_id)?>"> <?php echo(get_the_title($post_id))?></a></h2>
			</article>
		<?php }
		
	}
	/* !!!Стандартный вывод постов на главной странице */
	else{
	?>		
	
<div class="postBox-main"> 	
<!-- Начало .postBox -->
<?php if (have_posts()) : ?>
<?php $one = true; ?>
<?php while (have_posts()) : the_post(); ?>
<!-- Вывод поста -->
<article class="postBox <?php if($one == true) echo "one"; else echo "two"; ?>" id="postBox-<?php the_ID(); ?>">
<div class="postThumb"><a href="<?php the_permalink() ?>"><?php MultiPostThumbnails::the_post_thumbnail(get_post_type(),'secondary-image', NULL, 'post-list-img'); ?></a></div>
<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
</article>
<!-- Конец .postBox --> 
<?php $one = !$one; if ($one) echo '<br clear=all>'; ?>
<?php endwhile; endif; ?>
</div>   	
	
	
<?php }
	do_action( 'omega_after_content' ); ?>
</main><!-- .content -->
<?php get_footer(); ?>