<?php
/* Load Omega theme framework. */
require ( trailingslashit( get_template_directory() ) . 'lib/framework.php' );
new Omega();

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */

function omega_theme_setup() {

	//remove_theme_mods();

	/* Load omega functions */
	require get_template_directory() . '/lib/functions/hooks.php';

	add_theme_support( 'title-tag' ); 
	
	/* Load scripts. */
	add_theme_support( 
		'omega-scripts', 
		array( 'comment-reply' ) 
	);
	
	add_theme_support( 'post-thumbnails' );
	add_image_size('post-list-img', 300, 300, true);
	if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            // Replace [YOUR THEME TEXT DOMAIN] below with the text domain of your theme (found in the theme's `style.css`).
            'label' => __( 'Secondary Image', '[YOUR THEME TEXT DOMAIN]'),
            'id' => 'secondary-image',
            'post_type' => 'post'
        )
    );
	}
	
	add_filter( 'excerpt_length', function(){
	return 20;} );
	
	add_theme_support( 'omega-theme-settings' );

	add_theme_support( 'omega-content-archives' );
		
	/* implement editor styling, so as to make the editor content match the resulting post output in the theme. */
	add_editor_style();

	/* Support pagination instead of prev/next links. */
	add_theme_support( 'loop-pagination' );	

	/* Add default posts and comments RSS feed links to <head>.  */
	add_theme_support( 'automatic-feed-links' );

	/* Enable wraps */
	add_theme_support( 'omega-wraps' );

	/* Enable custom post */
	add_theme_support( 'omega-custom-post' );
	
	/* Enable custom css */
	add_theme_support( 'omega-custom-css' );
	
	/* Enable custom logo */
	add_theme_support( 'omega-custom-logo' );

	/* Enable child themes page */
	add_theme_support( 'omega-child-page' );

	add_theme_support( 'woocommerce' );

	/* Handle content width for embeds and images. */
	omega_set_content_width( 700 );
	
}
add_action( 'after_setup_theme', 'omega_theme_setup' );



/**
 * Widget API: WP_Widget_Categories class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Core class used to implement a Categories widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class WP_Widget_Custom_Categories extends WP_Widget {

	/**
	 * Sets up a new Categories widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_categories_custom',
			'description' => __( 'A list or dropdown of categories.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'categories', __( 'Categories' ), $widget_ops );
	}

	/**
	 * Outputs the content for the current Categories widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @staticvar bool $first_dropdown
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Categories widget instance.
	 */
	public function widget( $args, $instance ) {
		static $first_dropdown = true;

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Categories' );

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$c = ! empty( $instance['count'] ) ? '1' : '0';
		$h = ! empty( $instance['hierarchical'] ) ? '1' : '0';
		$d = ! empty( $instance['dropdown'] ) ? '1' : '0';

		echo $args['before_widget'];

		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

			/* вывод списка рубрик */
$args = array(
	'parent' => 0,
	'hide_empty' => 0,
	'exclude' => '', // ID рубрики, которую нужно исключить
	'number' => '0',
	'orderby' => 'count',
	'order' => 'DESC',
	'taxonomy' => 'category', // таксономия, для которой нужны изображения
	'pad_counts' => true
);
$catlist = get_categories($args); // получаем список рубрик
echo '<ul>'; 
foreach($catlist as $categories_item){

	// получаем данные из плагина Taxonomy Images
	$terms = apply_filters('taxonomy-images-get-terms', '', array(
		'taxonomy' => 'category' // таксономия, для которой нужны изображения
		));

	if (!empty($terms)){
		foreach((array)$terms as $term){
			if ($term->term_id == $categories_item->term_id){
				// выводим изображение рубрики
				print '<li class="cat_img"><a href="' . esc_url(get_term_link($term, $term->taxonomy)) . '" title="Нажмите, чтобы перейти в рубрику">' . wp_get_attachment_image($term->image_id, 'thumbnail');
                               echo '</a>';
				}
			}
		}
	
	
	}
echo '</ul>';
echo '<ul>';

		/**
		 * Filters the arguments for the Categories widget.
		 *
		 * @since 2.8.0
		 * @since 4.9.0 Added the `$instance` parameter.
		 *
		 * @param array $cat_args An array of Categories widget options.
		 * @param array $instance Array of settings for the current widget.
		 */
		/*wp_list_categories( apply_filters( 'widget_categories_args', $cat_args, $instance ) );*/
echo '</ul>';
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Categories widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
		$instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;

		return $instance;
	}

	/**
	 * Outputs the settings form for the Categories widget.
	 *
	 * @since 2.8.0
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = sanitize_text_field( $instance['title'] );
		$count = isset($instance['count']) ? (bool) $instance['count'] :false;
		$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
		$dropdown = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('dropdown'); ?>" name="<?php echo $this->get_field_name('dropdown'); ?>"<?php checked( $dropdown ); ?> />
		<label for="<?php echo $this->get_field_id('dropdown'); ?>"><?php _e( 'Display as dropdown' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
		<label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label></p>
		<?php
	}
}

function true_top_posts_widget_load() {
	register_widget( 'WP_Widget_Custom_Categories' );
}
add_action( 'widgets_init', 'true_top_posts_widget_load' );


add_action('init','random_add_rewrite');
function random_add_rewrite() {
       global $wp;
       $wp->add_query_var('random');
       add_rewrite_rule('random/?$', 'index.php?random=1', 'top');
}
 
add_action('template_redirect','random_template');
function random_template() {
	   //Рандом для рубрик
       if (get_query_var('random') == 1) {
		   	$infocat = get_the_category();
			$info = $infocat[0]->cat_ID;   
		   	   $list_args = array( 'numberposts' => 1, 'category' => $info, 'post_type' => 'post', 'orderby' => 'rand' );
               $posts = get_posts($list_args);
               foreach($posts as $post) {
                       $link = get_permalink($post);
               }
               wp_redirect($link,307);
               exit;
       }
		//Рандом для главной страницы
       if(get_query_var('random') == 2) {
		   $posts = get_posts('post_type=post&orderby=rand&numberposts=1');
		   foreach($posts as $post) {
                      $link = get_permalink($post);
               }
               wp_redirect($link,307);
               exit;
	   }
	//Рандом для перехода с передачей имени рубрики
	if(get_query_var('random') != ""){
		$mycat = get_category_by_slug(get_query_var('random'));
		$info = $mycat->cat_ID;   
		$list_args = array( 'numberposts' => 1, 'category' => $info, 'post_type' => 'post', 'orderby' => 'rand' );
        $posts = get_posts($list_args);
        foreach($posts as $post) {
                $link = get_permalink($post);
        }
        wp_redirect($link,307);
        exit;		
	}
}

//Популярное
add_action('init','popular_add_rewrite');
function popular_add_rewrite() {
       global $wp;
       $wp->add_query_var('popular');
       add_rewrite_rule('popular/?$', 'index.php?popular=1', 'top');
}
 
add_action('template_redirect','random_template');
function popular_template() {
	   //Рандом для рубрик
       echo("POPULAR!");
}