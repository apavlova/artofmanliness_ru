<?php
/**
 * The template for displaying Archive pages.
 *
 * @package Omega
 */

get_header(); ?>
<div class="new-sidebar">
	<a href="index.php?random=1" class="new-sidebar-btn"><h4>Рандом</h4></a>
	<a href="<?php get_category_link(get_query_var('cat')); ?>" class="new-sidebar-btn"><h4>Новое</h4></a>
	<a href="../../index.php?popular=<?php echo get_queried_object()->slug; ?>" class="new-sidebar-btn"><h4>Популярное</h4></a>
</div>
	<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
		<?php 
		do_action( 'omega_before_content' );
		//do_action( 'omega_content' );
		?>
		
		<?php 
if( is_category() )
	echo("<h3>");
	echo get_queried_object()->name;		
	echo("</h3>");
		
if (have_posts()) : ?>
 <?php $one = true; ?>
 <?php while (have_posts()) : the_post(); ?>
  
<!-- Начало .postBox -->
  
<article class="postBox <?php if($one == true) echo "one" ?>" id="postBox-<?php the_ID(); ?>">
<div class="postThumb"><a href="<?php the_permalink() ?>"><?php MultiPostThumbnails::the_post_thumbnail(get_post_type(),'secondary-image', NULL, 'post-list-img'); ?></a></div>
	
<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
  
</article>
  
<!-- Конец .postBox -->
  
<?php $one = !$one; if ($one) echo '<br clear=all>'; ?>
  
<?php endwhile; endif; ?>
		
		
		<?php do_action( 'omega_after_content' );
		?>	
	</main><!-- .content -->
<?php get_footer(); ?>