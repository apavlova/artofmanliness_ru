<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Omega
 */
get_header(); ?>
<div class="new-sidebar">
	<a href="index.php?random=2" class="new-sidebar-btn"><h4>Рандом</h4></a>
	<a href="/wordpress" class="new-sidebar-btn"><h4>Новое</h4></a>
	<a href="index.php?popular=1" class="new-sidebar-btn"><h4>Популярное</h4></a>
</div>
	<main class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
		<?php do_action( 'omega_before_content' ); ?>
		<?php do_action( 'omega_content' ); ?>		
		<?php do_action( 'omega_after_content' ); ?>

	</main><!-- .content -->	
<?php get_footer(); ?>